# Tweetcloud Backend

A simple REST-like API providing data to the [Tweetcloud App](https://gitlab.com/msvana/phd/tweetcloud-frontend).

## Endpoints

### `/companies`

Provides a list of companies currently stored in the database

### `/wordcounts/{symbol}/{startDate}/{endDate}`

Provides word counts for a given company in a given date range. This data is used to render
a word cloud

- `{symbol}` should be an **uppercase** stock symbol
- `{startDate}` and `{endDate}` should be in `YYYY-MM-DD` format

### `tweetcounts/{symbol}`

Provides daily tweet counts for a given symbol. This data is  used to render a timeline
with daily tweet numbers.

- `{symbol}` should be an **uppercase** stock symbol

## Build and deployment

The easiest way to build and deploy the service is by using the attached `Dockerfile`. An image
can be build easily by running:

```shell
$ docker build -t tweetcloud-backend .
```

After the image is build we can easily start a new container, for example:

```shell
docker run -d --restart="always" -p 8080:8080 --name=tweetcloud-backend tweetcloud-backend
```

The service is now available at `http://[your-ip]/wordcloud/`