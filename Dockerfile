# Build App with Maven
FROM maven:3-openjdk-17 AS builder
COPY . /app
WORKDIR /app
RUN mvn clean package
RUN ls

# Create a webapp container
FROM tomcat:9.0-jdk17-openjdk
COPY --from=builder /app/target/wordcloud.backend-0.1.war /usr/local/tomcat/webapps/wordcloud.war