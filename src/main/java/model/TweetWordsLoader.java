package model;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

public class TweetWordsLoader extends BaseDbModel {
    private static final String GET_COUNTS_QUERY = """
        SELECT word, SUM(count) AS total_count
        FROM tweets_words t
        JOIN companies c ON t.company_id = c.id
        WHERE c.symbol = ? AND t.date >= ? AND t.date <= ?
        GROUP BY word
        ORDER BY total_count DESC
        LIMIT 200
        """;

    private StopWordsList stopWordsList;

    public TweetWordsLoader(Connection dbConnection) {
        super(dbConnection);

        try {
            this.stopWordsList = StopWordsList.getInstance();
        } catch (IOException e) {
            e.printStackTrace();
            this.stopWordsList = null;
        }
    }

    public ArrayList<WordCount> getWordCounts(String symbol, Date startDate, Date endDate) throws SQLException {

        var stmt = this.dbConnection.prepareStatement(TweetWordsLoader.GET_COUNTS_QUERY);
        stmt.setString(1, symbol);
        stmt.setDate(2, new java.sql.Date(startDate.getTime()));
        stmt.setDate(3, new java.sql.Date(endDate.getTime()));

        String word;
        var resultSet = stmt.executeQuery();
        var wordCounts = new ArrayList<WordCount>();

        while (resultSet.next()) {
            word = resultSet.getString("word");

            if(!word.matches("^[a-zA-Z\\d]*$") || symbol.toLowerCase().equals(word)) {
                continue;
            }

            if(this.stopWordsList != null && this.stopWordsList.contains(word)) {
                continue;
            }

            var wordCount = new WordCount(
                resultSet.getString("word"),
                resultSet.getInt("total_count"));
            wordCounts.add(wordCount);
        }

        stmt.close();
        return wordCounts;
    }
}
