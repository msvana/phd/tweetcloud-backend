package model;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.SortedMap;
import java.util.TreeMap;

public class CompaniesLoader extends BaseDbModel {
    private static final String GET_COMPANIES_QUERY = "SELECT * FROM tc_companies";
    private static final String GET_TWEET_COUNTS_QUERY = """
        SELECT t.created_at AS date, COUNT(*) as num_tweets
        FROM tweets t JOIN companies c ON c.id = t.company_id
        WHERE c.symbol = ?
        GROUP BY t.created_at
        ORDER BY t.created_at
        """;
    private static final String COMPANY_EXISTS_QUERY = "SELECT COUNT(*) AS cnt FROM companies WHERE symbol = ? LIMIT 1";

    public CompaniesLoader(Connection dbConnection) {
        super(dbConnection);
    }

    public boolean companyExists(String symbol) throws SQLException {
        var stmt = this.dbConnection.prepareStatement(CompaniesLoader.COMPANY_EXISTS_QUERY);
        stmt.setString(1, symbol);
        var resultSet = stmt.executeQuery();
        var companyExists = false;

        while (resultSet.next()) {
            companyExists = resultSet.getInt("cnt") > 0;
        }

        return companyExists;
    }

    public ArrayList<Company> getCompanies() throws SQLException {
        var stmt = this.dbConnection.createStatement();
        var resultSet = stmt.executeQuery(CompaniesLoader.GET_COMPANIES_QUERY);

        var companies = new ArrayList<Company>();

        while (resultSet.next()) {
            var company = new Company(
                resultSet.getInt("company_id"),
                resultSet.getString("symbol"),
                resultSet.getDate("first_tweet_date"),
                resultSet.getDate("last_tweet_date"));
            companies.add(company);
        }

        stmt.close();
        return companies;
    }

    public SortedMap<Date, Integer> getTweetCounts(String symbol) throws SQLException {
        var stmt = this.dbConnection.prepareStatement(CompaniesLoader.GET_TWEET_COUNTS_QUERY);
        stmt.setString(1, symbol);
        var resultSet = stmt.executeQuery();
        var tweetCounts = new TreeMap<Date, Integer>();

        while (resultSet.next()) {
            var date = resultSet.getDate("date");
            var tweetCount = resultSet.getInt("num_tweets");
            tweetCounts.put(date, tweetCount);
        }

        stmt.close();
        return tweetCounts;
    }
}
