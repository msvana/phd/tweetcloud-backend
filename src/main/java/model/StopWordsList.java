package model;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class StopWordsList {
    private static StopWordsList instance = null;
    private ArrayList<String> stopWords;

    private StopWordsList() throws IOException {
        this.stopWords = new ArrayList<>();

        var stopWordsStream = getClass().getClassLoader().getResourceAsStream("stop_words_english.txt");
        var reader = new BufferedReader(new InputStreamReader(stopWordsStream));

        String line;
        while ((line = reader.readLine()) != null) {
            this.stopWords.add(line);
        }
    }

    public static StopWordsList getInstance() throws IOException {
        if (StopWordsList.instance == null) {
            StopWordsList.instance = new StopWordsList();
        }

        return StopWordsList.instance;
    }

    public boolean contains(String word) {
        return this.stopWords.contains(word);
    }

}
