package model;

import java.sql.Connection;

public class BaseDbModel {
    protected Connection dbConnection;

    public BaseDbModel(Connection dbConnection) {
        this.dbConnection = dbConnection;
    }
}
