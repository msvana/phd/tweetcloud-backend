package model;

import java.io.Serializable;
import java.util.Date;

public record Company(
    int companyId,
    String symbol,
    Date firstTweetDate,
    Date lastTweetDate
) implements Serializable {}
