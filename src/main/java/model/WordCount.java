package model;

public record WordCount(
    String word,
    int count
) {}
