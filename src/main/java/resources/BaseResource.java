package resources;

import javax.ws.rs.WebApplicationException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class BaseResource {
    private static Connection dbConnection = null;

    public static Connection getConnection() {
        if (BaseResource.dbConnection == null) {
            try {
                BaseResource.dbConnection = BaseResource.createDbConnection();
            } catch (SQLException e) {
                throw new WebApplicationException("DB_CONNECTION_ERROR", 500);
            }
        }

        return BaseResource.dbConnection;
    }


    private static Connection createDbConnection() throws SQLException{
        try {
            Class.forName("org.postgresql.Driver");
        } catch (java.lang.ClassNotFoundException e) {
            throw new WebApplicationException("DB_DRIVER_MISSING", 500);
        }

        return DriverManager.getConnection(
            Config.postgresJdbc, Config.postgresUser, Config.postgresPassword);
    }
}
