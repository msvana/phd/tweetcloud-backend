package resources.exceptions.handlers;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class WebApplicationExceptionHandler implements ExceptionMapper<WebApplicationException> {

    @Override
    public Response toResponse(WebApplicationException e) {
        var message = e.getMessage();
        var responseJson = "{\"status\": \"ERROR\", \"error\": \"" + message + "\"}";
        return Response.status(e.getResponse().getStatus())
            .entity(responseJson).type(MediaType.APPLICATION_JSON).build();
    }
}
