package resources;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import model.CompaniesLoader;
import model.TweetWordsLoader;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Path("/")
public class ApiResource extends BaseResource {

    @GET
    @Path("companies")
    @Produces(MediaType.APPLICATION_JSON)
    public Response companies() throws SQLException, JsonProcessingException {
        var companiesLoader = new CompaniesLoader(ApiResource.getConnection());
        var companies = companiesLoader.getCompanies();
        var mapper = new ObjectMapper();
        return Response.status(200).entity(mapper.writeValueAsString(companies)).build();
    }

    @GET
    @Path("wordcounts/{symbol}/{startDate}/{endDate}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response tweets(
            @PathParam("symbol") String symbol, @PathParam("startDate") String startDateString,
            @PathParam("endDate") String endDateString) throws SQLException, JsonProcessingException {
        var dateFormatter = new SimpleDateFormat("yyyy-dd-MM");
        Date startDate, endDate;

        try {
            startDate = dateFormatter.parse(startDateString);
            endDate = dateFormatter.parse(endDateString);
        } catch (ParseException e) {
            throw new WebApplicationException("INVALID_DATE_FORMAT", 400);
        }

        if (startDate.after(endDate)) {
            throw new WebApplicationException("START_DATE_AFTER_END_DATE", 409);
        }

        var tweetWordsLoader = new TweetWordsLoader(ApiResource.getConnection());
        var wordCounts = tweetWordsLoader.getWordCounts(symbol, startDate, endDate);
        var mapper = new ObjectMapper();
        return Response.status(200).entity(mapper.writeValueAsString(wordCounts)).build();
    }

    @GET
    @Path("tweetcounts/{symbol}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response tweetcounts(@PathParam("symbol") String symbol) throws SQLException, JsonProcessingException {
        var companiesLoader = new CompaniesLoader(ApiResource.getConnection());

        if (!companiesLoader.companyExists(symbol)) {
            throw new WebApplicationException("COMPANY_NOT_FOUND", 404);
        }

        var tweetCounts = companiesLoader.getTweetCounts(symbol);
        var mapper = new ObjectMapper();
        return Response.status(200).entity(mapper.writeValueAsString(tweetCounts)).build();
    }
}
