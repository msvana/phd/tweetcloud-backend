package resources;

public class Config {

    public static final String postgresJdbc = "jdbc:postgresql://10.8.0.1:10003/stockdb";
    public static final String postgresUser = "postgres";
    public static final String postgresPassword = "postgresDevPassword";

}
